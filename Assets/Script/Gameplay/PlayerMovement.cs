﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;   

public class PlayerMovement : MonoBehaviour
{
    private Vector3 change;
    private int jumpVelocity = 8;
    private int speed = 25;
    private int life;

    private float pointTimer = 1.0f;
    private float skillCD;
    float invulTimer = 1f;
    public int score;

    private Text scoreText;
    private Text lifeText;
    private Text cooldownText;

    private GameObject hillSpawn;


    private Color oldColor;

    public PhotonView photonView;

    void Start()
    {
        Color playerColor = new Color(Random.Range(0, 200)/255f, Random.Range(0, 200)/255f, Random.Range(0, 255)/255f);
        oldColor = playerColor;
        gameObject.GetComponent<Renderer>().material.color = oldColor;

        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        lifeText = GameObject.Find("LifeText").GetComponent<Text>();
        cooldownText = GameObject.Find("Cooldown").GetComponent<Text>();

        score = 0;
        life = 20;
        skillCD = 10;
        photonView = GetComponent<PhotonView>();

        hillSpawn = GameObject.Find("KingOfTheHill");

       

    }
    void Update()
    {
        if (photonView.IsMine)
        {
            Vector3 m_Input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            GetComponent<Rigidbody>().MovePosition(transform.position + m_Input * Time.deltaTime * speed);

            

            if (Input.GetKeyDown(KeyCode.Space))
            {
                IsJumping();
            }
            if (gameObject.tag == "InvulCrown")
            {
                invulCollision();
            }
            if (gameObject.tag == "Crown")
            {
                PointIncrease();
                TPCooldown();
            }
            else
            {
                LifeDecrease();
            }
        }
    }

    public void IsJumping()
    {
        GetComponent<Rigidbody>().velocity = Vector3.up * jumpVelocity;
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Crown" && gameObject.tag != "Crown")
        {
            if (collision.gameObject.name == "Crown")
            {
                photonView.RPC("DestroyCrown", RpcTarget.All);
            }
            else
            {
                collision.gameObject.GetComponent<PlayerMovement>().photonView.RPC("ChangePlayerTag", RpcTarget.All);
                collision.gameObject.GetComponent<PlayerMovement>().photonView.RPC("ChangeNormalColor", RpcTarget.All);
            }

            photonView.RPC("ChangeGoldColor", RpcTarget.All);
            gameObject.transform.position = hillSpawn.transform.position;
            photonView.RPC("ChangeInvulCrownTag", RpcTarget.All);
            

            skillCD = 10;
            speed = 15;

        }
        
    }

    void PointIncrease()
    {
        if(pointTimer > 0)
        {
            pointTimer -= Time.deltaTime;
        }
        else
        {
            pointTimer = 1.0f;
            score++;
            if(life < 20)
            {
                life++;
            }
            scoreText.text = "Score : " + score.ToString();
            lifeText.text = "Life : " + life.ToString();

        }

    }

    void LifeDecrease()
    {
        if (pointTimer > 0)
        {
            pointTimer -= Time.deltaTime;
        }
        else
        {
            pointTimer = 1.0f;
            life--;
            lifeText.text = "Life : " + life.ToString();
            if (life == 0)
            {
                life = 20;
                score = 0;
            }
        }

    }

    void TPCooldown()
    {
        
        if (skillCD > 0)
        {
            skillCD -= Time.deltaTime;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                skillCD = 20;
                gameObject.transform.position = hillSpawn.transform.position;
            }
            
        }
        int cdIntText = (int)skillCD;
        cooldownText.text = "Cooldown : " + cdIntText.ToString();
    }

    void invulCollision()
    {

        if (invulTimer > 0)
        {
            invulTimer -= Time.deltaTime;
        }
        else
        {
            photonView.RPC("ChangeCrownTag", RpcTarget.All);
            invulTimer = 1;
        }
    }

    [PunRPC]
    public void ChangeGoldColor()
    {
        Color goldColor = new Color(1, 1, 0.3f);
        gameObject.GetComponent<Renderer>().material.color = goldColor;
    }
    [PunRPC]
    public void ChangeNormalColor()
    {
        gameObject.GetComponent<Renderer>().material.color = gameObject.GetComponent<PlayerMovement>().oldColor;
        gameObject.tag = "Player";
    }
    [PunRPC]
    public void ChangeInvulCrownTag()
    {
        gameObject.tag = "InvulCrown";
    }
    [PunRPC]
    public void ChangeCrownTag()
    {
        gameObject.tag = "Crown";
    }
    [PunRPC]
    public void ChangePlayerTag()
    {
        gameObject.tag = "Player";
    }
    [PunRPC]
    public void DestroyCrown()
    {
        Destroy(GameObject.Find("Crown"));
    }
}
