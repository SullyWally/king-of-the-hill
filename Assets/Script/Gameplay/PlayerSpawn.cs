﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSpawn : MonoBehaviour
{
    public GameObject playerPrefabs;

    public float minX ;
    public float maxX ;
    public float minZ ;
    public float maxZ ;

    private void Start()
    {
        Vector3 randomPos = new Vector3(Random.Range(minX, maxX), -15, Random.Range(minZ, maxZ));
        PhotonNetwork.Instantiate(playerPrefabs.name, randomPos, Quaternion.identity);
        
    }
}
